# Curso Técnicas do Shell

O curso Técnicas do Shell é um curso permanente com conteúdo e recursos em texto em vídeos livres e gratuitos.

<!--more-->

## Para quem é o curso

Para todos, sem distição de nível de conhecimento, que queiram aprender e exercitar suas habilidades de resolver problemas com o shell do GNU/Linux.

## Recursos didáticos

- [Repositório git para o material desenvolvido no curso](https://codeberg.org/blau_araujo/tecnicas-do-shell)
- [Espaço para dúvidas e discussões sobre os assuntos do curso](https://codeberg.org/blau_araujo/tecnicas-do-shell/issues)
- [Sala de bate-papo sobre o conteúdo do curso (e de todos os outros)](https://gitter.im/blau_araujo/community)
- [Cortes das transmissões ao vivo pelo Youtube](https://youtube.com/playlist?list=PLXoSGejyuQGrUv3tv46y3FSrP-pz3fSv_)

## Conteúdo

O conteúdo será produzido à medida em que chegarem dúvidas pelos nossos canais e pelas issues deste repositório e será disponibilizado na forma de artigos ou vídeos, de acordo com os casos. Além diso, eu vou propor desafios que serão discutidos e trabalhados nas issues, podendo resultar em encontros marcados pelo Jitsi para discussões ao vivo.

## Duração do curso

- É um curso permanente.

## Inscrições

Não há inscrições, o curso é aberto, livre e gratuito.

## Valor da inscrição

- **Não há inscrições**, mas você pode contribuir apoiando o nosso trabalho ou adquirindo [convites para participar pelo Jitsi nas transmissões ao vivo](lives/README.md#convites-para-o-jitsi).

## Formas de apoio

- [Apoio regular no Apoia.se](https://apoia.se/debxpcursos)
- [Doações pelo PicPay](https://app.picpay.com/user/blauaraujo)
- Chave PIX: pix@blauaraujo.com
- [Adquirindo a versão impressa do Pequeno Manual do Programador GNU/Bash](https://blauaraujo.com/2022/02/17/versao-impressa-do-pequeno-manual-do-programador-gnu-bash/)
