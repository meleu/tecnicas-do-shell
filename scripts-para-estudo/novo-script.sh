#!/usr/bin/env bash

editor='nano'

read d m y <<< $(date '+%d %b %Y')

header="#!/usr/bin/env bash
# -----------------------------------------------------------------------------
# Script   : $1
# Descrição: 
# Versão   : 
# Data     : ${m^} $d, $y
# Licença  : GNU/GPL v3.0
# -----------------------------------------------------------------------------
# Copyright (C) $y Blau Araujo  <blau@debxp.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------
# Uso: 
# -----------------------------------------------------------------------------
"

usage='Uso: ns ARQUIVO'
err[1]='Erro: nome do arquivo não informado!'
err[2]='Erro: arquivo já existe!'
exiting='Saindo...'

die() {
	printf '%s\n%s\n\n%s\n' "${err[$1]}" "$usage" "$exiting"
	exit $1
}

[[ -z $1 ]] && die 1
[[ -f $1 ]]	&& die 2

echo "$header" >> $1
chmod +x $1
$editor $1
