#!/usr/bin/env bash
# ------------------------------------------------------------------------------
# Script   : qn
# Descrição: QuickNote - simple notes manager
# Versão   : 0.0.5
# Data     : 19/03/2022
# Licença  : GNU/GPL v3.0
# ------------------------------------------------------------------------------
# Copyright (C) 2020-2022  debxp.org <https://debxp.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------
# Uso: qn [OPTIONS]
# ------------------------------------------------------------------------------

version='0.0.5'

# strings ----------------------------------------------------------------------

prompt_mode[0]='New note (leave blank or empty to cancel)'
prompt_mode[1]='Edit note (leave without changes to cancel)'
prompt_mode[2]='Delete this note (y/N)?'

prompt_file=$'\nSelect where to save your notes: '

str_note='Note:'
str_date='Date:'
str_edit='Last edit:'
str_exit="Quitting..."
str_done="Done!"
str_help="type '$0 -h' for help."
str_sel='Search'
str_cancel='Cancel'
str_first_use="Notes file created.\nType 'qn -n' to create a new note."

error[0]='Canceled!'
error[1]="Wrong number of arguments: $str_help"
error[2]="Invalid option ($1): $str_help"
error[3]='Missing dependency:'
error[4]='There are no notes to list.'
error[5]='Notes file not selected.'

usage="QuickNote (qn) $version - simple notes manager.

USAGE
    qn [OPTIONS]

OPTIONS
    -l    List all notes (default).
    -s    Search and select note.
    -n    New note.
    -e    Edit note.
    -d    Delete note.
    -v    Version.
    -h    This help
"

copyright="QuickNote (qn) $version - simple notes manager.

Copyright (C) 2022  debxp.org <https://debxp.org>
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Written by Blau Araujo
"

# functions --------------------------------------------------------------------

check_depends() {
    local dep
    for dep; do
	command -v $1 > /dev/null || die 3 $dep
    done
}

die() {
    printf '\n\e[1m%s %s\e[0m\n' "${error[$1]} $2"
    printf '%s\n\n' "$str_exit"
    exit $1
}

draw_line() {
    local line
    printf -v line "%$(tput cols)s"
    printf '\e[30;1m%s\e[0m' "${line// /─}"
}

print_note() {
    draw_line
    printf '\e[35;1m%s\e[0m %s '  "$str_date" "$QN_DATE"
    printf '\e[35;1m%s\e[0m %s\n' "$str_edit" "$QN_EDIT"
    printf '\e[35;1m%s\e[0m %s\n' "$str_note" "$QN_TEXT" | fold -sw 70
    draw_line
}

list_notes() {
    grep -Env '^\s+$' $QN_FILE
}

select_note() {
    list_notes | $menu_cmd "${menu_opt[@]}"
}

get_data() {
    ((QN_EMPTY)) && die 4
    local note data
    (($#)) && note="$*" || note=$(select_note)
    data=$(sed -E 's/^([0-9]+):([^ ]+) (.*)$/\1 \2 \3/' <<< $note)
    read QN_LINE QN_DATE QN_EDIT QN_TEXT <<< "$data"
}

new_date() {
    date +'%d.%m.%y-%H:%M'
}

edit_note() {
    local prompt note
    prompt=$(printf '\e[36;1m%s\n> \e[0m' "${prompt_mode[QN_MODE]}")
    read -rei "$QN_TEXT" -p "$prompt" note
    [[ ${note//[[:blank:]]/} && "$note" != "$QN_TEXT" ]] && QN_EMPTY=0 || die 0
    QN_EDIT=$(new_date)
    ((QN_MODE)) || QN_DATE=$QN_EDIT
    get_data ${QN_LINE:-0}:$QN_DATE $QN_EDIT "$note"
}

new_mode() {
    edit_note
    echo $QN_DATE $QN_EDIT $QN_TEXT >> $QN_FILE
    print_note
    printf '\n\e[32;1m%s\e[0m\n' "$str_done"
}

edit_mode() {
    get_data
    [[ $QN_TEXT ]] || die 0
    edit_note
    sed -i "${QN_LINE}s|.*|$QN_DATE $QN_EDIT ${QN_TEXT//|/\\|}|" $QN_FILE
    print_note
    printf '\n\e[32;1m%s\e[0m\n' $str_done
}

delete_mode() {
    get_data
    print_note
    [[ $QN_DATE ]] || die 0
    printf '\e[36;1m%s\e[0m ' "${prompt_mode[2]}"
    read -n1
    [[ ${REPLY,,} == [sy] ]] && sed -i "${QN_LINE}d" $QN_FILE || die 0
    printf '\n\e[32;1m%s\e[0m\n' $str_done
}

select_mode() {
    get_data
    [[ $QN_TEXT ]] && print_note $QN_DATE $QN_EDIT "$QN_TEXT"
}

check_file_contents() {
    [[ -s $QN_FILE ]]; QN_EMPTY=$?
}


qnotes_file() {
    local qnotes_rc qnotes_xdg opt
    
    qnotes_rc=$HOME/.qnotes
    qnotes_xdg=$HOME/.config/qnotes

    if [[ -f $qnotes_rc ]];then
	QN_FILE=$qnotes_rc
    elif [[ -f $qnotes_xdg ]]; then
	QN_FILE=$qnotes_xdg
    else
	PS3="$prompt_file"
	echo
	select opt in $qnotes_rc $qnotes_xdg $str_cancel; do
	    case $REPLY in
		1) >> $qnotes_rc;;
		2) mkdir -p ${qnotes_xdg%/*}; >> $qnotes_xdg;;
		3) die 5
	    esac
	    break
	done
	QN_FILE=$opt
	printf '\n\e[1m%b\e[m\n\n' "$str_first_use"
	exit
    fi
}

# main -------------------------------------------------------------------------

check_depends fzf

qnotes_file

check_file_contents

[[ $# -gt 1 ]] && die 1

menu_cmd='fzf'

menu_opt=(
    --height=15
    --border
    --reverse -e -i
    --tiebreak=begin
    --prompt="$str_sel: "
)

case $1 in
       -n) # New mode...
	   QN_MODE=0
	   new_mode
	   ;;
       -e) # Edit mode...
	   QN_MODE=1
	   edit_mode
	   ;;
       -d) # Delete mode...
	   QN_MODE=2
	   delete_mode
	   ;;
    -l|'') # List mode...
	   list_notes
	   ;;
       -s) # Search/select mode...
	   select_mode
	   ;;
       -h) # Help...
	   echo "$usage"
	   exit
	   ;;
       -v) # Version/copyright...
	   echo "$copyright"
	   exit
	   ;;
        *) # Invalid options...
	   die 2
esac
