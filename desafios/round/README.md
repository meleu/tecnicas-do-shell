# Arredondamento para inteiros

Desafio para a aula do dia 29/04/2022 do curso **Técnicas do Shell**.

## Desafio

Dado um número decimal válido, arredondá-lo para um valor aproximado inteiro de uma das três formas:

#### "Round" (arredondar)

O valor arredondado para o inteiro **mais próximo**, seja ele maior ou menor.

```
round -3.1 → -3
round -3.3 → -3
round -3.5 → -4
round -3.7 → -4

round 3.1 → 3
round 3.3 → 3
round 3.5 → 4
round 3.7 → 4
```

#### "Floor" (piso)

O valor arredondado para o menor valor inteiro anterior.

```
floor -3.1 → -4
floor -3.3 → -4
floor -3.5 → -4
floor -3.7 → -4

floor 3.1 → 3
floor 3.3 → 3
floor 3.5 → 3
floor 3.7 → 3
```

#### "Ceil" (teto)

O valor arredondado para o maior valor inteiro seguinte.

```
ceil -3.1 → -3
ceil -3.3 → -3
ceil -3.5 → -3
ceil -3.7 → -3

ceil 3.1 → 4
ceil 3.3 → 4
ceil 3.5 → 4
ceil 3.7 → 4
```

## Requisitos

- O script deverá conter três funções, uma para cada método de arredondamento.
- O tipo de arredondamento a ser aplicado será definido na linha do comando.
- As três funções devem ser independentes e reaproveitáveis em outros scripts.
- Utilizar apenas comandos internos do Bash.
- Possíveis erros de invocação do script devem ser tratados.

## Números válidos

- Inteiros positivos e negativos: `3`, `234`, `-5`...
- Decimais positivos e negativos separados por vírgula: `-4,2`, `123,456`...
- Decimais positivos e negativos separados por ponto: `4.23`, `-123.456`...

## Uso do scripts

O script será chamado de `round.sh` e será utilizado da seguinte forma:

```
:~$ round.sh NÚMERO           # Executa a função 'round'.
:~$ round.sh round NÚMERO     # Executa a função 'round'.
:~$ round.sh ceil NÚMERO      # Executa a função 'ceil'.
:~$ round.sh floor NÚMERO     # Executa a função 'ceil'.
:~$ round.sh -h ou --help     # Exibe ajuda.
:~$ round.sh -v ou --version  # Exibe versão e licença.
```

