# TÉCNICAS DO SHELL #1
# O desafio do terapeuta quântico (ELIZA)

+ Introdução

- Trabalhar com método
- Transformar problemas em conhecimento
- Problemas raramente vêm com enunciados
- Bons enunciados trazem respostas em si mesmos

+ Desenvolvimento

- O que é ELIZA?
- Parte 1 - O problema (descrição)
- Parte 2 - Técnicas envolvidas (modelagem)
- Parte 3 - Testando os códigos (demonstração)
- Parte 4 - O que aprendemos (consolidação)
