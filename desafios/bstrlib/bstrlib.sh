#!/usr/bin/env bash
# bstrlib.sh
############
#
# Desafio publicado em:
# https://codeberg.org/blau_araujo/tecnicas-do-shell/src/branch/main/desafios/bstrlib
#
### Criérios de avaliação
# - Observância das condições e descrições.
# - Data da entrega (quanto antes, mais pontos).
# - Clareza e riqueza das explicações dos comentários.
# - Síntese das soluções (soluções abreviadas valem mais pontos).
# - Engenhosidade.
#
## Condições
# - Soluções 100% em Bash.
# - Uma função não pode depender das outras.
# - Funções que pedirem variáveis entre aspas como argumentos
#   referem-se aos nomes dessas variáveis, não às suas expansões.
# - As formas de uso e as descrições devem ser seguidas à risca.
# - Deve ser utilizado o arquivo original do script alterando apenas
#   o conteúdo das funções.
# - Cada linha relevante das funções deve ser explicada em comentário.
# - As soluções devem ser apresentadas nos repositório próprios dos
#   autores no Codeberg: https://codeberg.org
# - Não serão aceitas soluções apresentadas em outras plataformas.
# - O link do repositório deve ser informado na issue relativa a este desafio.
#
# MEUS COMENTÁRIOS
##################
#
# - Mesmo sabendo que engenhosidade e soluções abreviadas valem mais pontos,
#   eu priorizei clareza de código. Acredito que essa é uma das skills mais
#   importante de um programador e jamais devemos abrir mão disso.
#
# - No meu "coding-style" pessoal as variáveis são nomeadas usando-se camelCase
#
# - Em funções onde usamos `declare -n` precisamos tomar muito cuidado com
#   colisão de nomes de variáveis! Portanto nestes casos eu sempre uso nomes
#   como "__nome_da_funcao_variavelEmCamelCase". É, eu sei que fica feio, mas
#   evita surpresinhas desagradáveis com a colisão.
#
# - Muitas das técnicas de manipulação de string que uso aqui eu aprendi
#   no "Pure Bash Bible", disponível em
#   <https://github.com/dylanaraps/pure-bash-bible>.
#   LEITURA ALTAMENTE RECOMENDADA! (em inglês)

readonly REGEX_IS_VALID_VARIABLE_NAME='^[_[:alpha:]][_[:alnum:]]*$'
readonly REGEX_IS_ARRAY='^declare\ -[^\ ]*a\ '
readonly REGEX_IS_INTEGER='^[[:digit:]]+$'

# -----------------------------------------------------------------------------
# Função 1: trim()
# -----------------------------------------------------------------------------
# Uso      : trim 'STRING' 'VAR' ['CARACTERE|CLASSE']
# Descrição: Remove caracteres do inínicio e do fim da string
#            e atribui a uma variável.
#            Os caracteres padrão são os definidos pela
#            classe nomeada [:space:].
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
trim() {
  # isValidArguments()
  [[ $# -ge 2 && $# -le 3 && $2 =~ ${REGEX_IS_VALID_VARIABLE_NAME} ]] || return 1

  local __trim_string="$1"
  declare -n __trim_variable="$2"
  local __trim_pattern="${3:-[:space:]}" # ${var:-aqui fica o valor default}
  local __trim_patternToDelete

  # definindo o padrão que será deletado à esquerda
  #
  # "${var%%padrao}"     -> remove do FINAL de var a maior combinacao
  #                         que casa com o 'padrao'
  #
  # "${var%%[^padrao]*}" -> remove do FINAL de var a maior combinacao
  #                         que NÃO casa com o 'padrao' seguida de
  #                         qualquer coisa (por conta do asterisco).
  #
  # Na prática, isso vai me dar exatamente a substring que eu quero remover
  # da esquerda.
  __trim_patternToDelete="${__trim_string%%[^${__trim_pattern}]*}"

  # deletando o padrão à esquerda
  # "${var#padrao}"      -> remove do começo de var a primeira
  #                         ocorrência do padrão
  __trim_variable="${__trim_string#"${__trim_patternToDelete}"}"

  # definindo o padrão que será deletado à direita
  #
  # "${var##padrao}"     -> remove do INÍCIO de var a maior combinacao
  #                         que casa com o 'padrao'
  # "${var##*[^padrao]}" -> remove do INÍCIO de var a maior combinacao
  #                         de "qualquer coisa" que termine com 'padrão'.
  #
  # Na prática, isso vai me dar exatamente a substring que eu quero remover
  # da direita.
  __trim_patternToDelete="${__trim_variable##*[^${__trim_pattern}]}"

  # deletando o padrão à direita
  # "${var%padrao}"      -> remove do final de var a primeira
  #                         ocorrência do padrão
  __trim_variable="${__trim_variable%"${__trim_patternToDelete}"}"
}

# -----------------------------------------------------------------------------
# Função 2: ltrim()
# -----------------------------------------------------------------------------
# Uso      : ltrim 'STRING' 'VAR' ['CARACTERE|CLASSE']
# Descrição: Remove caracteres do início da string e atribui
#            a uma variável.
#            Os caracteres padrão são os definidos pela
#            classe nomeada [:space:].
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
#
# -----------------------------------------------------------------------------
# MEUS COMENTÁRIOS:
#
# A explicação sobre essa solução já foi abordada nos comentários do trim().
#
# -----------------------------------------------------------------------------
ltrim() {
  # isValidArguments()
  [[ $# -ge 2 && $# -le 3 && $2 =~ ${REGEX_IS_VALID_VARIABLE_NAME} ]] || return 1

  local __ltrim_string="$1"
  declare -n __ltrim_var="$2"
  local __ltrim_pattern="${3:-[:space:]}"
  local __ltrim_patternToDelete="${__ltrim_string%%[!${__ltrim_pattern}]*}"

  __ltrim_var="${__ltrim_string#"${__ltrim_patternToDelete}"}"
}

# -----------------------------------------------------------------------------
# Função 3: rtrim()
# -----------------------------------------------------------------------------
# Uso      : rtrim 'STRING' 'VAR' ['CARACTERE|CLASSE']
# Descrição: Remove caracteres do fim da string e atribui
#            a uma variável.
#            Os caracteres padrão são os definidos pela
#            classe nomeada [:space:].
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
#
# -----------------------------------------------------------------------------
# MEUS COMENTÁRIOS:
#
# A explicação sobre essa solução já foi abordada nos comentários do trim().
#
# -----------------------------------------------------------------------------
rtrim() {
  # isValidArguments()
  [[ $# -ge 2 && $# -le 3 && $2 =~ ${REGEX_IS_VALID_VARIABLE_NAME} ]] || return 1

  local __rtrim_string="$1"
  declare -n __ltrim_var="$2"
  local __rtrim_pattern="${3:-[:space:]}"
  local __rtrim_patternToDelete="${__rtrim_string##*[!${__rtrim_pattern}]}"

  __ltrim_var="${__rtrim_string%"${__rtrim_patternToDelete}"}"
}

# -----------------------------------------------------------------------------
# Função 4: strim()
# -----------------------------------------------------------------------------
# Uso      : strim 'STRING' 'VAR' ['CARACTERE|CLASSE']
# Descrição: Remove caracteres do início e do fim da string,
#            trunca ocorrências múltiplas do mesmo caractere
#            na string (sqeeze) e atribui a uma variável.
#            Os caracteres padrão são os definidos pela
#            classe nomeada [:space:].
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
#
# -----------------------------------------------------------------------------
# MEUS COMENTÁRIOS:
#
# Nesta função eu repito o código usado no trim(). Fiz isso pois o desafio
# exige que uma função não faça referência a outras.
#
# Não recomendo que você faça isso na sua vida profissional!
#
# Na "vida real", se você ficar espalhando código repetido em vários lugares
# diferentes, depois quando você precisar corrigir e/ou melhorar seu código,
# você vai ficar p*%@ da vida de ter que alterar vários arquivos. Com certeza
# você vai esquecer de algum. Ou pior, se você já está em outro trampo, o
# coleguinha que vai ter que dar manutenção no seu código vai ficar #xatiado
#
# -----------------------------------------------------------------------------
# shellcheck disable=1009,1072,1073,1020
strim() {
  # isValidArguments()
  [[ $# -ge 2 && $# -le 3 && $2 =~ ${REGEX_IS_VALID_VARIABLE_NAME} ]] || return 1

  local __strim_string="$1"
  declare -n __strim_variable="$2"
  local __strim_pattern="${3:-[:space:]}"
  local __strim_patternToDelete
  local patternPreviouslyFound='false'
  local currentChar
  local i

  # ISSO AQUI DEVERIA SER UMA CHAMADA AO trim()
  # ---------------------------------------------------------------------------
  # essa maçaroca de código nada mais do que o trim(), já explicado acima.
  __strim_patternToDelete="${__strim_string%%[^${__strim_pattern}]*}"
  __strim_variable="${__strim_string#"${__strim_patternToDelete}"}"
  __strim_patternToDelete="${__strim_variable##*[^${__strim_pattern}]}"
  __strim_variable="${__strim_variable%"${__strim_patternToDelete}"}"
  # ---------------------------------------------------------------------------

  # ISSO AQUI DEVERIA SER UMA CHAMADA AO squeeze()
  # ---------------------------------------------------------------------------
  # a lógica abaixo será explicado no squeeze(), logo a seguir.
  __strim_string="${__strim_variable}"
  __strim_variable=''

  for ((i = 0; i < ${#__strim_string}; i++)); do
    currentChar="${__strim_string:i:1}"

    if [[ ${currentChar} == [${__strim_pattern}] ]]; then
      [[ ${patternPreviouslyFound} == 'true' ]] && continue
      patternPreviouslyFound='true'
    else
      patternPreviouslyFound='false'
    fi
    __strim_variable+="${currentChar}"
  done
  # ---------------------------------------------------------------------------
}

# -----------------------------------------------------------------------------
# Função 5: squeeze()
# -----------------------------------------------------------------------------
# Uso      : squeeze 'STRING' 'VAR' ['CARACTERE|CLASSE']
# Descrição: Trunca para uma as ocorrências múltiplas do
#            mesmo caractere na string e atribui a uma
#            variável.
#            Os caracteres padrão são os definidos pela
#            classe nomeada [:space:].
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
#
# -----------------------------------------------------------------------------
# MEUS COMENTÁRIOS:
#
# A minha solução aqui envolve fazer um loop iterando em cada caractere da
# string. Sempre que preciso fazer isso em bash na minha cabeça fica soando
# um alarme me dizendo que não estou usando a ferramenta correta!
#
# Fiz alguns testes de performance aqui (usando hyperfine) e observei
# o seguinte:
#
# Observação 1:
# Se não fosse obrigatório tratar de [:classes:], não precisaríamos do loop
# e poderíamos resolver de maneira extremamente performática com algo assim:
#
# shopt -s extglob
# var="${string//+("${pattern}")/${pattern}}"
#
# Observação 2:
# Se não fosse obrigatório ser bash puro, poderíamos usar o comando 'tr'
# e, mesmo criando um processo novo, isso seria bem mais performático do
# que fazer um loop percorrendo todos os caracteres da string. A solução
# seria tipo assim:
#
# var="$(tr --squeeze-repeats "[${pattern}]" <<< "${string}")"
#
# -----------------------------------------------------------------------------
# shellcheck disable=1009,1072,1073,1020
squeeze() {
  # isValidArguments()
  [[ $# -ge 2 && $# -le 3 && $2 =~ ${REGEX_IS_VALID_VARIABLE_NAME} ]] || return 1

  local __squeeze_originalString="$1"
  declare -n __squeeze_var="$2"
  local __squeeze_pattern="${3:-[:space:]}"
  local patternPreviouslyFound='false'
  local currentChar
  local i

  # limpando qqr conteúdo que porventura esteja na variável
  __squeeze_var=''

  for ((i = 0; i < ${#__squeeze_originalString}; i++)); do
    currentChar="${__squeeze_originalString:i:1}"

    if [[ "${currentChar}" == [${__squeeze_pattern}] ]]; then
      [[ "${patternPreviouslyFound}" == 'true' ]] && continue
      patternPreviouslyFound='true'
    else
      patternPreviouslyFound='false'
    fi
    __squeeze_var+="${currentChar}"
  done
}

# -----------------------------------------------------------------------------
# Função 6: strlen()
# -----------------------------------------------------------------------------
# Uso      : strlen 'VAR'
# Descrição: Imprime o número de caracteres da string em VAR.
# Saída    : O número de caracteres da string.
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
strlen() {
  local string="${!1:?strlen: faltou informar nome da variável}"
  echo "${#string}"
}

# -----------------------------------------------------------------------------
# Função 7: explode()
# -----------------------------------------------------------------------------
# Uso      : explode 'STRING' 'ARRAY' ['SEPARADOR|CLASSE']
# Descrição: Quebra a string nas ocorrências de um separador
#            e atribui cada parte a um elemento de um vetor.
#            O separador padrão é a vírgula.
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
explode() {
  # isValidArguments()
  [[ $# -ge 2 && $# -le 3 && $2 =~ ${REGEX_IS_VALID_VARIABLE_NAME} ]] || return 1

  local __explode_string="$1"
  declare -n __explode_array="$2"
  local __explode_separator="${3:-,}"

  IFS="${__explode_separator}" read -ra __explode_array <<< "${1}"

  # tratando o último elemento (se for vazio, o bash vai ignorar)
  [[ "${__explode_string}" == *${__explode_separator} ]] && __explode_array+=('')
  return 0
}

# -----------------------------------------------------------------------------
# Função 8: implode()
# -----------------------------------------------------------------------------
# Uso      : implode 'VETOR' 'VAR' ['SEPARADOR']
# Descrição: Une os elementos de um vetor em uma string
#            intercalados por um separador e atribui
#            a uma variável.
#            O separador padrão é a vírgula.
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
implode() {
  [[ $# -ge 2 && $# -le 3 ]] || return 1

  # isArray()
  [[ $1 =~ ${REGEX_IS_VALID_VARIABLE_NAME} ]] || return 1
  [[ $(declare -p "$1" 2> /dev/null) =~ ${REGEX_IS_ARRAY}${1} ]] || return 1

  declare -n __implode_array="$1"
  declare -n __implode_variable="$2"
  local __implode_separator="${3:-,}"

  # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  # !!! alerta: hack de legibilidade não muito clara !!!
  # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  # Transformando os elementos de __implode_array em argumentos passados à implode()
  set -f                         # desabilita globbing
  set -- "${__implode_array[@]}" # altera os argumentos
  set +f                         # reabilita globbing

  local IFS="${__implode_separator}"
  __implode_variable="$*"
}

# -----------------------------------------------------------------------------
# Função 9: in_array()
# -----------------------------------------------------------------------------
# Uso      : in_array 'STRING|PADRÃO' 'VETOR'
# Descrição: Verifica se uma string ou padrão (FNPM)
#            acontece em algum dos elementos do vetor.
# Saída    : Não produz dados na saída padrão.
#            Define o vetor global ARRAY_MATCHES com os
#            índices dos elementos onde houve casamento.
#            Sai com sucesso se a string for encontrada.
#            Sai com erro 1 se a string não for encontrada
#            ou erro 2 se houver erro de parâmetros.
in_array() {
  [[ $# -eq 2 ]] || return 2 # retorna 2 se houver erro de parâmetros

  local __in_array_pattern="$1"
  declare -n __in_array_array="$2"
  local i
  declare -ga ARRAY_MATCHES=()

  for ((i = 0; i < ${#__in_array_array[@]}; i++)); do
    [[ ${__in_array_array[i]} == ${__in_array_pattern} ]] && ARRAY_MATCHES+=("${i}")
  done

  # retorna 1 se "pattern" não for encontrado
  [[ ${#ARRAY_MATCHES[@]} -gt 0 ]]
}

# -----------------------------------------------------------------------------
# Função 10: in_string()
# -----------------------------------------------------------------------------
# Uso      : in_string 'STRING|PADRÃO' 'VAR'
# Descrição: Verifica se uma substring ou padrão (FNPM)
#            acontece em algum ponto da string em VAR.
# Saída    : Não produz dados na saída padrão.
#            Define o vetor global STRING_POS com os
#            índices dos caracteres na string a partir dos
#            quais houve casamento.
#            Sai com sucesso se a substring for encontrada.
#            Sai com erro 1 se a substring não for encontrada
#            ou erro 2 se houver erro de parâmetros.
in_string() {
  [[ $# -eq 2 ]] || return 2

  local __in_string_pattern="$1"
  declare -n __in_string_var="$2"
  local i
  declare -ga STRING_POS=()

  # se substring não for encontrada, aborta logo (evita passar pelo loop)
  [[ ${__in_string_var} == *${__in_string_pattern}* ]] || return 1

  for ((i = 0; i < ${#__in_string_var}; i++)); do
    [[ "${__in_string_var:i}" == ${__in_string_pattern}* ]] && STRING_POS+=("${i}")
  done

  # precisa retornar sucesso explicitamente, caso contrário
  # retornará o status do último teste dentro do "for"
  return 0
}

# -----------------------------------------------------------------------------
# Função 11: str_repeat()
# -----------------------------------------------------------------------------
# Uso      : str_repeat 'STRING|CHAR' REPETIÇÕES 'VAR'
# Descrição: Define uma variável associada ao resultado das
#            repetições de um caractere ou de uma string.
# Saída    : Não produz dados na saída padrão.
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
str_repeat() {
  [[ $# -eq 3 && $3 =~ ${REGEX_IS_VALID_VARIABLE_NAME} ]] || return 1
  [[ $2 =~ ${REGEX_IS_INTEGER} ]] || return 1

  local __str_repeat_string="$1"
  local __str_repeat_repetitions="$2"
  local __str_repeat_output="$3"
  local __str_repeat_sequenceNumbers
  local i

  __str_repeat_sequenceNumbers="$(
    for ((i = 0; i < __str_repeat_repetitions; i++)); do
      echo "$i"
    done
  )"

  # Alternativa 1: usando `eval`:
  #
  # OBSERVAÇÃO IMPORTANTE!
  # Não use eval! Lembre-se "eval is evil!". Mas se você for teimoso e quiser usar
  # mesmo assim, certifique-se de sanitizar/validar os argumentos.
  #
  # __str_repeat_sequenceNumbers="$(eval echo {1.."${__str_repeat_repetitions}"})"

  # Alternativa 2: usando `seq` (não é 100% bash)
  #
  # __str_repeat_sequenceNumbers="$(seq 1 "${__str_repeat_repetitions}")"

  # Truques do printf:
  #
  # - Usando `printf -v var ...` a saída do printf é atribuída em $var
  #
  # - Printf processa todos argumentos, repetindo a formatação se necessário.
  #
  # - O format '%.0s' não imprime nada, mas "consome" itens da lista de argumentos.
  #
  # - "${string//%/%%}" - sanitizando a variável $string caso seja passado um '%'.
  #
  # - Use '--' pra dizer ao printf que não quer passar mais opções. Dessa forma
  #   podemos passar '-' como o caractere a ser repetido.
  printf -v "${__str_repeat_output}" -- \
    "${__str_repeat_string//%/%%}%.0s" ${__str_repeat_sequenceNumbers}
}

# -----------------------------------------------------------------------------
# Função 12: count()
# -----------------------------------------------------------------------------
# Uso      : count 'VETOR'
# Descrição: Define a variável global ARRAY_COUNT com
#            a quantidade de elementos de um vetor.
# Saída    : Não produz dados na saída padrão.
#            Sai com erro 1 se a variável não for um
#            vetor ou com erro 2 se houver erro de
#            parâmetros.
count() {
  # isArray()
  [[ $1 =~ ${REGEX_IS_VALID_VARIABLE_NAME} ]] || return 1
  [[ $(declare -p "$1" 2> /dev/null) =~ ${REGEX_IS_ARRAY} ]] || return 1

  declare -n __count_array="$1"
  declare -g ARRAY_COUNT="${#__count_array[@]}"
}
