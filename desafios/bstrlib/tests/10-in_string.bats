#!/usr/bin/env bats

setup() {
  source "${BATS_TEST_DIRNAME}"/../bstrlib.sh
}

@test 'encontra substrings na variável' {
  local var="pera uva maçã 'salada mista' maçã uva pera"

  in_string uva var
  [[ "${#STRING_POS[@]}" -eq 2 ]]
  [[ "${var:${STRING_POS[0]}}" == uva* ]]
  [[ "${var:${STRING_POS[1]}}" == uva* ]]

  in_string 'maçã' var
  [[ "${#STRING_POS[@]}" -eq 2 ]]
  [[ "${var:${STRING_POS[0]}}" == maçã* ]]
  [[ "${var:${STRING_POS[1]}}" == maçã* ]]
}

@test 'encontra substrings usando coringa' {
  local var="pera uva maçã mamão 'salada mista'"

  in_string 'ma*' var
  [[ "${#STRING_POS[@]}" -eq 2 ]]
  [[ "${var:${STRING_POS[0]}}" == maçã* ]]
  [[ "${var:${STRING_POS[1]}}" == mamão* ]]

  in_string '*' var
  [[ "${#STRING_POS[@]}" -eq 34 ]]
}

