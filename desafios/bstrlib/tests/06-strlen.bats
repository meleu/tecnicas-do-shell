#!/usr/bin/env bats

setup() {
  source "${BATS_TEST_DIRNAME}"/../bstrlib.sh
}

@test 'número de caracteres em \"áéíóú\" = 5' {
  local var='áéíóú'
  [[ $(strlen var) -eq 5 ]]
}


