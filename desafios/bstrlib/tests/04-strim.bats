#!/usr/bin/env bats

setup() {
  source "${BATS_TEST_DIRNAME}"/../bstrlib.sh
}

@test 'não aceita variável com nome inválido' {
  strim string .nome.invalido || true
  strim string 1nome.invalido || true
  strim string '#nome.invalido' || true
}

@test 'remove espaços excessivos em \"  visite  meleu  ponto  sh  \"' {
  local var="  visite  meleu  ponto  sh  "
  strim "${var}" var
  echo "'$var'"
  [[ "${var}" == "visite meleu ponto sh" ]]
}

@test 'limpa excesso de asteriscos em \"**visite **meleu ponto sh** agora mesmo!**\"' {
  local var="**visite **meleu ponto sh** agora mesmo!**"
  strim "${var}" var '*'
  echo "'$var'"
  [[ "${var}" == "visite *meleu ponto sh* agora mesmo!" ]]
}

@test 'limpa números nas extremidades e trunca os repetidos em \"123visite111meleu222ponto333sh123\"' {
  local var="123visite111meleu222ponto333sh123"
  strim "${var}" var '[:digit:]'
  echo "'$var'"
  [[ "${var}" == "visite1meleu2ponto3sh" ]]
}

