#!/usr/bin/env bats

setup() {
  source "${BATS_TEST_DIRNAME}"/../bstrlib.sh
}

@test 'não aceita variável com nome inválido' {
  rtrim string .nome.invalido || true
  rtrim string 1nome.invalido || true
  rtrim string '#nome.invalido' || true
}

@test 'remove espaços a direita de "  string  "' {
  local var='  string  '
  rtrim "${var}" var
  [[ "${var}" == "  string" ]]
}

@test 'limpa a direita de "**string**"' {
  local var='**string**'
  rtrim "${var}" var '*'
  [[ "${var}" == "**string" ]]
}

@test 'remove os números a direita de "123string123" usando [:digit:]' {
  local var='123string123'
  rtrim "${var}" var '[:digit:]'
  [[ "${var}" == "123string" ]]
}

