#!/usr/bin/env bats

setup() {
  source "${BATS_TEST_DIRNAME}"/../bstrlib.sh
}

@test 'explode \"pera,uva,maçã,salada mista\"' {
  local array=()
  explode "pera,uva,maçã,salada mista" array ','
  echo "${array[*]/#/- }"
  [[ "${array[*]/#/- }" == "- pera - uva - maçã - salada mista" ]]
}

@test 'array com elementos vazios \",pera,uva,,maçã,salada mista,\"' {
  local array=()
  explode ",pera,uva,,maçã,salada mista," array ','
  [[ "${array[*]/%/,}" == ", pera, uva, , maçã, salada mista, ," ]]
}
