#!/usr/bin/env bats

setup() {
  source "${BATS_TEST_DIRNAME}"/../bstrlib.sh
}

@test 'encontra elemento no array' {
  local array=(pera uva maçã 'salada mista' maçã uva pera)

  in_array uva array
  [[ "${#ARRAY_MATCHES[@]}" -eq 2 ]]
  [[ "${array[${ARRAY_MATCHES[0]}]}" == uva ]]
  [[ "${array[${ARRAY_MATCHES[1]}]}" == uva ]]

  in_array 'maçã' array
  [[ "${#ARRAY_MATCHES[@]}" -eq 2 ]]
  [[ "${array[${ARRAY_MATCHES[0]}]}" == 'maçã' ]]
  [[ "${array[${ARRAY_MATCHES[1]}]}" == 'maçã' ]]
}

@test 'encontra elementos usando coringa' {
  local array=(pera uva maçã mamão 'salada mista')

  in_array 'ma*' array
  [[ "${#ARRAY_MATCHES[@]}" -eq 2 ]]
  [[ "${array[${ARRAY_MATCHES[0]}]}" == 'maçã' ]]
  [[ "${array[${ARRAY_MATCHES[1]}]}" == 'mamão' ]]

  in_array '*' array
  [[ "${#ARRAY_MATCHES[@]}" -eq 5 ]]
  [[ "${array[${ARRAY_MATCHES[0]}]}" == 'pera' ]]
  [[ "${array[${ARRAY_MATCHES[1]}]}" == 'uva' ]]
  [[ "${array[${ARRAY_MATCHES[2]}]}" == 'maçã' ]]
  [[ "${array[${ARRAY_MATCHES[3]}]}" == 'mamão' ]]
  [[ "${array[${ARRAY_MATCHES[4]}]}" == 'salada mista' ]]
}

