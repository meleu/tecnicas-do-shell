#!/usr/bin/env bats

setup() {
  source "${BATS_TEST_DIRNAME}"/../bstrlib.sh
}

@test 'implode array (pera uva maçã \"salada mista\")' {
  local array=(pera uva maçã 'salada mista')
  local var
  implode  array var ','
  echo "'$var'"
  [[ "${var}" == "pera,uva,maçã,salada mista" ]]
}

@test 'quebra se primeiro argumento não é um array' {
  ! implode invalidArray invalidVar
}
 
