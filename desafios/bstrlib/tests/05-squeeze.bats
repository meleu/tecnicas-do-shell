#!/usr/bin/env bats

setup() {
  source "${BATS_TEST_DIRNAME}"/../bstrlib.sh
}

@test 'não aceita variável com nome inválido' {
  squeeze string .nome_invalido || true
  squeeze string 1nome.invalido_ || true
  squeeze string '_#nome.invalido_' || true
}

@test 'trunca espaços excessivos em \"  visite  meleu  ponto  sh  \"' {
  local var="  visite  meleu  ponto  sh  "
  squeeze "${var}" var
  [[ "${var}" == " visite meleu ponto sh " ]]
}

@test 'trunca excesso de asteriscos em \"**visite **meleu ponto sh** agora mesmo!**\"' {
  local var="**visite **meleu ponto sh** agora mesmo!**"
  squeeze "${var}" var '*'
  [[ "${var}" == "*visite *meleu ponto sh* agora mesmo!*" ]]
}

@test 'trunca números repetidos em \"123visite111meleu222ponto333sh123\"' {
  local var="123visite111meleu222ponto333sh123"
  squeeze "${var}" var '[:digit:]'
  [[ "${var}" == "1visite1meleu2ponto3sh1" ]]
}

