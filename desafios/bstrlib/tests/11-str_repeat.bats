#!/usr/bin/env bats

setup() {
  source "${BATS_TEST_DIRNAME}"/../bstrlib.sh
}

@test 'repete uma string' {
  local var
  str_repeat char 5 var
  [[ "${var}" == charcharcharcharchar ]]
}

@test 'repete - hífem -' {
  local var
  str_repeat - 5 var
  [[ "${var}" == '-----' ]]
}

@test 'repete sinal de % porcentagem %' {
  local var
  str_repeat % 5 var
  [[ "${var}" == '%%%%%' ]]
}

@test 'falha ao tentar explorar uso do eval' {
  local var
  ! str_repeat = '$(echo "vc foi h4ck34d0!" >&2; exit 1)' var
}
