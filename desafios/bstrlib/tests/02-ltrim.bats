#!/usr/bin/env bats

setup() {
  source "${BATS_TEST_DIRNAME}"/../bstrlib.sh
}

@test 'não aceita variável com nome inválido' {
  ltrim string .nome.invalido || true
  ltrim string 1nome.invalido || true
  ltrim string '#nome.invalido' || true
}

@test 'remove espaços a esquerda de "  string  "' {
  local var='  string  '
  ltrim "${var}" var
  [[ "${var}" == "string  " ]]
}

@test 'limpa a esquerda de "**string**"' {
  local var='**string**'
  ltrim "${var}" var '*'
  [[ "${var}" == "string**" ]]
}

@test 'remove os números a esquerda de "123string123" usando [:digit:]' {
  local var='123string123'
  ltrim "${var}" var '[:digit:]'
  [[ "${var}" == "string123" ]]
}
