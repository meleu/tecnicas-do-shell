#!/usr/bin/env bats

setup() {
  source "${BATS_TEST_DIRNAME}"/../bstrlib.sh
}

@test 'não aceita variável com nome inválido' {
  trim string .nome.invalido || true
  trim string 1nome.invalido || true
  trim string '#nome.invalido' || true
}

@test 'remove espaços' {
  local var='  string  '
  trim "${var}" var
  [[ "${var}" == "string" ]]
}

@test 'limpa **string**' {
  local var='**string**'
  trim "${var}" var '*'
  [[ "${var}" == "string" ]]
}

@test 'limpa 123string123 usando [:digit:]' {
  local var='123string123'
  trim "${var}" var '[:digit:]'
  [[ "${var}" == "string" ]]
}
