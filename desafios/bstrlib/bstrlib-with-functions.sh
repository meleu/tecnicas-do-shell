#!/usr/bin/env bash
# bstrlib.sh
############
#
# Desafio publicado em:
# https://codeberg.org/blau_araujo/tecnicas-do-shell/src/branch/main/desafios/bstrlib
#
### Criérios de avaliação
# - Observância das condições e descrições.
# - Data da entrega (quanto antes, mais pontos).
# - Clareza e riqueza das explicações dos comentários.
# - Síntese das soluções (soluções abreviadas valem mais pontos).
# - Engenhosidade.
#
## Condições
# - Soluções 100% em Bash.
# - Uma função não pode depender das outras.
# - Funções que pedirem variáveis entre aspas como argumentos
#   referem-se aos nomes dessas variáveis, não às suas expansões.
# - As formas de uso e as descrições devem ser seguidas à risca.
# - Deve ser utilizado o arquivo original do script alterando apenas
#   o conteúdo das funções.
# - Cada linha relevante das funções deve ser explicada em comentário.
# - As soluções devem ser apresentadas nos repositório próprios dos
#   autores no Codeberg: https://codeberg.org
# - Não serão aceitas soluções apresentadas em outras plataformas.
# - O link do repositório deve ser informado na issue relativa a este desafio.

# shellcheck disable=2295,2034

readonly REGEX_VALID_VARNAME='^[_[:alpha:]][_[:alnum:]]*$'
readonly REGEX_IS_ARRAY='^declare\ -[^\ ]*a\ '

# -----------------------------------------------------------------------------
# Função 1: trim()
# -----------------------------------------------------------------------------
# Uso      : trim 'STRING' 'VAR' ['CARACTERE|CLASSE']
# Descrição: Remove caracteres do inínicio e do fim da string
#            e atribui a uma variável.
#            Os caracteres padrão são os definidos pela
#            classe nomeada [:space:].
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
trim() {
  # isValidArguments()
  [[ $# -ge 2 && $# -le 3 && $2 =~ ${REGEX_VALID_VARNAME} ]] || return 1

  local __trim_string="$1"
  local __trim_varName="$2"
  local __trim_pattern="${3:-[:space:]}"

  ltrim "${__trim_string}" __trim_string "${__trim_pattern}"
  rtrim "${__trim_string}" "${__trim_varName}" "${__trim_pattern}"
}

# -----------------------------------------------------------------------------
# Função 2: ltrim()
# -----------------------------------------------------------------------------
# Uso      : ltrim 'STRING' 'VAR' ['CARACTERE|CLASSE']
# Descrição: Remove caracteres do início da string e atribui
#            a uma variável.
#            Os caracteres padrão são os definidos pela
#            classe nomeada [:space:].
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
ltrim() {
  # isValidArguments()
  [[ $# -ge 2 && $# -le 3 && $2 =~ ${REGEX_VALID_VARNAME} ]] || return 1

  local __ltrim_string="$1"
  declare -n __ltrim_var="$2"
  local __ltrim_pattern="${3:-[:space:]}"
  local patternToDelete="${__ltrim_string%%[!${__ltrim_pattern}]*}"

  __ltrim_var="${__ltrim_string#"${patternToDelete}"}"
}

# -----------------------------------------------------------------------------
# Função 3: rtrim()
# -----------------------------------------------------------------------------
# Uso      : rtrim 'STRING' 'VAR' ['CARACTERE|CLASSE']
# Descrição: Remove caracteres do fim da string e atribui
#            a uma variável.
#            Os caracteres padrão são os definidos pela
#            classe nomeada [:space:].
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
rtrim() {
  # isValidArguments()
  [[ $# -ge 2 && $# -le 3 && $2 =~ ${REGEX_VALID_VARNAME} ]] || return 1

  local __rtrim_string="$1"
  declare -n __ltrim_var="$2"
  local __rtrim_pattern="${3:-[:space:]}"
  local patternToDelete="${__rtrim_string##*[!${__rtrim_pattern}]}"

  __ltrim_var="${__rtrim_string%"${patternToDelete}"}"
}

# -----------------------------------------------------------------------------
# Função 4: strim()
# -----------------------------------------------------------------------------
# Uso      : strim 'STRING' 'VAR' ['CARACTERE|CLASSE']
# Descrição: Remove caracteres do início e do fim da string,
#            trunca ocorrências múltiplas do mesmo caractere
#            na string (sqeeze) e atribui a uma variável.
#            Os caracteres padrão são os definidos pela
#            classe nomeada [:space:].
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
strim() {
  # isValidArguments()
  [[ $# -ge 2 && $# -le 3 && $2 =~ ${REGEX_VALID_VARNAME} ]] || return 1

  local __strim_string="$1"
  local __strim_varName="$2"
  local __strim_pattern="${3:-[:space:]}"

  trim "${__strim_string}" __strim_string "${__strim_pattern}"
  squeeze "${__strim_string}" "${__strim_varName}" "${__strim_pattern}"
}

# -----------------------------------------------------------------------------
# Função 5: squeeze()
# -----------------------------------------------------------------------------
# Uso      : squeeze 'STRING' 'VAR' ['CARACTERE|CLASSE']
# Descrição: Trunca para uma as ocorrências múltiplas do
#            mesmo caractere na string e atribui a uma
#            variável.
#            Os caracteres padrão são os definidos pela
#            classe nomeada [:space:].
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
# shellcheck disable=1009,1072,1073,1020
squeeze() {
  # isValidArguments()
  [[ $# -ge 2 && $# -le 3 && $2 =~ ${REGEX_VALID_VARNAME} ]] || return 1

  local originalString="$1"
  declare -n __squeeze_var="$2"
  local pattern="${3:-[:space:]}"
  local patternPreviouslyFound='false'
  local currentChar
  local i

  # limpando qqr conteúdo que porventura esteja na variável
  __squeeze_var=''

  for ((i = 0; i < ${#originalString}; i++)); do
    currentChar="${originalString:i:1}"

    if [[ ${currentChar} == [${pattern}] ]]; then
      [[ ${patternPreviouslyFound} == 'true' ]] && continue
      patternPreviouslyFound='true'
    else
      patternPreviouslyFound='false'
    fi
    __squeeze_var+="${currentChar}"
  done

}

# SOLUÇÕES ALTERNATIVAS
########################
#
# squeeze2()
# se não fosse necessário tratar de [:classes:] esta solução aqui
# resolveria e seria extremamente performática:
squeeze2() {
  # isValidArguments()
  [[ $# -ge 2 && $# -le 3 && $2 =~ ${REGEX_VALID_VARNAME} ]] || return 1

  local originalString="$1"
  declare -n __squeeze2_var="$2"
  local pattern="${3:- }" # valor default será um espaço

  shopt -s extglob
  __squeeze2_var="${originalString//+("${pattern}")/${pattern}}"
}

# squeeze3()
# Se não fosse obrigatório ser bash puro, essa solução usando
# o comando 'tr', mesmo criando um novo processo, seria bem mais
# performática do que aquele loop "for" percorrendo todos os
# caracteres da string:
squeeze3() {
  # isValidArguments()
  [[ $# -ge 2 && $# -le 3 && $2 =~ ${REGEX_VALID_VARNAME} ]] || return 1

  local originalString="$1"
  declare -n __squeeze3_var="$2"
  local pattern="${3:-[:space:]}"

  __squeeze3_var="$(tr --squeeze-repeats "[${pattern}]" <<< "${originalString}")"
}

# -----------------------------------------------------------------------------
# Função 6: strlen()
# -----------------------------------------------------------------------------
# Uso      : strlen 'VAR'
# Descrição: Imprime o número de caracteres da string em VAR.
# Saída    : O número de caracteres da string.
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
strlen() {
  local string="${!1:?strlen: faltou informar nome da variável}"
  echo "${#string}"
}

# -----------------------------------------------------------------------------
# Função 7: explode()
# -----------------------------------------------------------------------------
# Uso      : explode 'STRING' 'ARRAY' ['SEPARADOR|CLASSE']
# Descrição: Quebra a string nas ocorrências de um separador
#            e atribui cada parte a um elemento de um vetor.
#            O separador padrão é a vírgula.
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
explode() {
  # isValidArguments()
  [[ $# -ge 2 && $# -le 3 && $2 =~ ${REGEX_VALID_VARNAME} ]] || return 1

  local __explode_string="$1"
  declare -n __explode_array="$2"
  local __explode_separator="${3:-,}"

  IFS="${__explode_separator}" read -ra __explode_array <<< "${1}"

  # tratando o último elemento (se for vazio, o bash vai ignorar)
  [[ "${__explode_string}" == *${__explode_separator} ]] && __explode_array+=('')
  return 0
}

# -----------------------------------------------------------------------------
# Função 8: implode()
# -----------------------------------------------------------------------------
# Uso      : implode 'VETOR' 'VAR' ['SEPARADOR']
# Descrição: Une os elementos de um vetor em uma string
#            intercalados por um separador e atribui
#            a uma variável.
#            O separador padrão é a vírgula.
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
implode() {
  [[ $# -ge 2 && $# -le 3 ]] || return 1

  # isArray()
  [[ $1 =~ ${REGEX_VALID_VARNAME} ]] || return 1
  [[ $(declare -p "$1" 2> /dev/null) =~ ${REGEX_IS_ARRAY}${1} ]] || return 1

  declare -n __implode_array="$1"
  declare -n __implode_variable="$2"
  local __implode_separator="${3:-,}"

  # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  # !!! alerta: hack de legibilidade não muito clara !!!
  # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  # Transformando os elementos de __implode_array em argumentos passados à implode()
  set -f                         # desabilita globbing
  set -- "${__implode_array[@]}" # altera os argumentos
  set +f                         # reabilita globbing

  local IFS="${__implode_separator}"
  __implode_variable="$*"
}

# -----------------------------------------------------------------------------
# Função 9: in_array()
# -----------------------------------------------------------------------------
# Uso      : in_array 'STRING|PADRÃO' 'VETOR'
# Descrição: Verifica se uma string ou padrão (FNPM)
#            acontece em algum dos elementos do vetor.
# Saída    : Não produz dados na saída padrão.
#            Define o vetor global ARRAY_MATCHES com os
#            índices dos elementos onde houve casamento.
#            Sai com sucesso se a string for encontrada.
#            Sai com erro 1 se a string não for encontrada
#            ou erro 2 se houver erro de parâmetros.
in_array() {
  [[ $# -eq 2 ]] || return 2 # retorna 2 se houver erro de parâmetros

  local pattern="$1"
  declare -n __in_array_array="$2"
  local i
  declare -ga ARRAY_MATCHES=()

  for ((i = 0; i < ${#__in_array_array[@]}; i++)); do
    [[ ${__in_array_array[i]} == ${pattern} ]] && ARRAY_MATCHES+=("${i}")
  done

  # retorna 1 se "pattern" não for encontrado
  [[ ${#ARRAY_MATCHES[@]} -gt 0 ]]
}

# -----------------------------------------------------------------------------
# Função 10: in_string()
# -----------------------------------------------------------------------------
# Uso      : in_string 'STRING|PADRÃO' 'VAR'
# Descrição: Verifica se uma substring ou padrão (FNPM)
#            acontece em algum ponto da string em VAR.
# Saída    : Não produz dados na saída padrão.
#            Define o vetor global STRING_POS com os
#            índices dos caracteres na string a partir dos
#            quais houve casamento.
#            Sai com sucesso se a substring for encontrada.
#            Sai com erro 1 se a substring não for encontrada
#            ou erro 2 se houver erro de parâmetros.
in_string() {
  [[ $# -eq 2 ]] || return 2

  local pattern="$1"
  declare -n __in_string_var="$2"
  local i
  declare -ga STRING_POS=()

  # se substring não for encontrada, aborta logo (evita passar pelo loop)
  [[ ${__in_string_var} == *${pattern}* ]] || return 1

  for ((i = 0; i < ${#__in_string_var}; i++)); do
    [[ "${__in_string_var:i}" == ${pattern}* ]] && STRING_POS+=("${i}")
  done

  # precisa retornar sucesso explicitamente, caso contrário
  # retornará o status do último teste dentro do "for"
  return 0
}

# -----------------------------------------------------------------------------
# Função 11: str_repeat()
# -----------------------------------------------------------------------------
# Uso      : str_repeat 'STRING|CHAR' REPETIÇÕES 'VAR'
# Descrição: Define uma variável associada ao resultado das
#            repetições de um caractere ou de uma string.
# Saída    : Não produz dados na saída padrão.
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
str_repeat() {
  [[ $# -eq 3 && $1 =~ ${REGEX_VALID_VARNAME} ]] || return 1

  local string="$1"
  local repetitions="$2"
  local __str_repeat="$3" # <-- necessário evitar colisão de nomes aqui.

  # Usando `printf -v var ...` para evitar ter que gerar um novo
  # processo com `var=$(printf ...)`.
  printf -v "${__str_repeat}" "${string//%/%%}%.0s" $(seq 1 "${repetitions}")
}

# -----------------------------------------------------------------------------
# Função 12: count()
# -----------------------------------------------------------------------------
# Uso      : count 'VETOR'
# Descrição: Define o vetor global ARRAY_COUNT com a
#            quantidade de elementos de um vetor.
# Saída    : Não produz dados na saída padrão.
#            Sai com erro 1 se a variável não for um
#            vetor ou com erro 2 se houver erro de
#            parâmetros.
count() {
  # isArray()
  [[ $1 =~ ${REGEX_VALID_VARNAME} ]] || return 1
  [[ $(declare -p "$1" 2> /dev/null) =~ ${REGEX_IS_ARRAY} ]] || return 1

  declare -n __count_array="$1"
  declare -g ARRAY_COUNT="${#__count_array[@]}"
}
