# Apelidos e aspas

> Comentários e dúvidas na issue #1

Salve!

Surgiu uma dúvida no curso quantao ao que acontece com as regras de citação no exemplo abaixo:

```
alias nome='echo XPTO é XPTO'
```

Será que, por estar entre aspas simples e, ainda assim, ser identificado como um comando pelo shell, o `echo` estaria sujeito a algum tipo de exceção? Será que ele continua sendo, de algum modo, especial para o shell?

> Antes de ler minha resposta, não deixe de ler [este tópico sobre regras de citação](https://blauaraujo.com/shell/livro/05-depois_do_enter#regras_de_citacao_quoting), do curso Shell GNU.

Não muda nada aqui.

Neste comando, você está atribuindo uma *string* (cadeia de caracteres) ao identificador do apelido `nome`. Portanto, essa string, **onde tudo é literal por causa das aspas simples**, será a linha de comando que será executada quando `nome` for invocado, o que podemos antever com o comando:

```
:~$ alias nome
alias nome='echo XPTO é XPTO'
```

Executando:

```
:~$ nome
XPTO é XPTO
```

Mas, veja o que acontece no exemplo abaixo: 

```
:~$ alias nome='echo Meu nome é $var'
```

Consegue ver que `$var` foi recebida na definição do apelido (entre aspas simples) como a sequência literal de caracteres `$`, `v`, `a` e `r`, removendo o significado especial de `$`?

O resultado disso é que, **somente quando o apelido for expandido**, `$var` significará para o shell uma expansão da variável `$var`.

Portanto:

```
:~$ var=Jandson
:~$ nome
Meu nome é Jandson
:~$ var=Blau
:~$ nome
Meu nome é Blau
```

Porém, veja o que acontece quando eu utilizo aspas duplas:

```
:~$ alias nome="echo Meu nome é $var"
```

Considerando que a última atribuição de `var` foi `Blau`, observe:

```
:~$ var=Jandson
:~$ nome
Meu nome é Blau
:~$ var=João
:~$ nome
Meu nome é Blau
```

O que aconteceu?

Com as aspas duplas, a expansão de `$var` aconteceu **durante a criação do apelido**, ou seja, o apelido registrado foi:

```
:~$ alias nome
alias nome='echo Meu nome é Blau'
```

Percebeu a diferença?