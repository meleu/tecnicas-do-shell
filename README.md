# Técnicas do Shell

Repositório de recursos do curso Técnicas do Shell, [transmitido ao vivo pelo Youtube todas as quintas, às 20h UTC-3](lives/README.md).

![](https://blauaraujo.com/wp-content/uploads/2022/05/cafezinho-01.png)

## Transmissões ao vivo!

**A transmissão de hoje foi reagendada para a semana que vem, 2 de junho!**

- [Quinta, 02 de junho de 2022, às 20h UTC-3](lives/README.md)
- [Quinta, 19 de maio de 2022, às 20h UTC-3](https://codeberg.org/blau_araujo/tecnicas-do-shell/src/branch/main/lives/ep001-terapeuta-quantico-1.md)

## Para assistir aos cortes dos episódios

- [Playlist completa no Youtube](https://youtube.com/playlist?list=PLXoSGejyuQGrUv3tv46y3FSrP-pz3fSv_)

## Links importantes

* [Sobre o curso](sobre-o-curso.md)
* [Artigos escritos a partir das dúvidas](artigos/)
* [Dúvidas para gerar o conteúdo do curso (issues)](https://codeberg.org/blau_araujo/tecnicas-do-shell/issues)
* [Vídeos e textos para aprender o shell do GNU/Linux](https://codeberg.org/blau_araujo/para-aprender-shell)
* [Discussões sobre os tópicos de todos os nossos cursos (Gitter)](https://gitter.im/blau_araujo/community)

## Formas de apoio

* [Apoio mensal pelo Apoia.se](https://apoia.se/debxpcursos)
* [Doações pelo PicPay](https://app.picpay.com/user/blauaraujo)
* Doações via PIX: pix@blauaraujo.com
* [Versão impressa do Pequeno Manual do Programador GNU/Bash](https://blauaraujo.com/2022/02/17/versao-impressa-do-pequeno-manual-do-programador-gnu-bash/)
