# Técnicas do Shell - ao vivo

<!--more-->

## Nesta quinta (19/05/2022)

Soluções para o desafio do [terapeuta quântico (ELIZA)](https://codeberg.org/blau_araujo/tecnicas-do-shell/src/commit/92c755cc98834683da2ae46cf055c2c8030379c5/desafios/terapeuta).

## Cortes da gravação

- [Playlist no Youtube](https://youtube.com/playlist?list=PLXoSGejyuQGrUv3tv46y3FSrP-pz3fSv_).
- Somente os participantes pelo Jitsi terão acesso à gravação bruta depois da transmissão e até a publicação dos cortes.
- Os demais poderão assistir aos cortes dos momentos mais relevantes gratuitamente pelo Youtube.

## Convites para o Jitsi

- Todos os apoiadores regulares (Apoia.se) podem solicitar gratuitamente o link do convite pelo e-mail [blau@debxp.org](mailto:blau@debxp.org).
- Convite para quatro transmissões): R$150,00
- Convite para uma transmissão: R$50,00

### Formas de contribuição para obter o convite

- [Apoio regular no Apoia.se](https://apoia.se/debxpcursos)
- [Depósito no PicPay](https://app.picpay.com/user/blauaraujo)
- Chave PIX: pix@blauaraujo.com

**Não se esqueça de mandar um e-mail solicitando o convite para o Jitsi!**
